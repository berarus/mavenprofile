package projecttests.config;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.BeforeClass;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

/**
 * Created by Anastasia on 3/31/2016.
 */
public class BaseTest {

    @BeforeClass
    public static void setUp(){
        Configuration.browser = System.getProperty("driver.browser");
        Configuration.timeout = 10000;
    }

    @After
    public void takeScreen() throws IOException {
        screenshot();
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() throws IOException {
        File screenshot = Screenshots.takeScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }

}
