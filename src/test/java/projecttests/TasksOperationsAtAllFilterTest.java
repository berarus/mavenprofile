package projecttests;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import pages.TodoMVCPage;
import projecttests.categories.Buggy;
import projecttests.categories.Smoke;
import projecttests.config.BaseTest;

import static pages.TodoMVCPage.TaskType.ACTIVE;
import static pages.TodoMVCPage.TaskType.COMPLETED;


/**
 * Created by Anastasia on 3/21/2016.
 */
public class TasksOperationsAtAllFilterTest extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    @Category(Smoke.class)
    public void testEditAtAll() {
        page.givenAtAll(ACTIVE, "1", "2");

        page.confirmEditByEnter("2", "2 edited");
        page.assertTasks("1", "2 edited");
        page.assertItemsLeft(2);
    }

    @Test
    @Category(Smoke.class)
    public void testCompleteAllAtAll() {
        page.givenAtAll(ACTIVE, "1", "2");

        page.toggleAll();
        page.assertTasks("1", "2");
        page.assertItemsLeft(0);
    }

    @Test
    public void testReopenAtAll() {
        page.givenAtAll(COMPLETED, "1", "2");

        page.toggle("2");
        page.assertTasks("1", "2");
        page.assertItemsLeft(1);
    }

    @Test
    @Category(Smoke.class)
    public void testReopenAllAtAll() {
        page.givenAtAll(COMPLETED, "1", "2");

        page.toggleAll();
        page.assertTasks("1", "2");
        page.assertItemsLeft(2);
    }

    @Test
    @Category(Smoke.class)
    public void testClearCompletedAtAll() {
        page.givenAtAll(page.aTask("1", COMPLETED),
                page.aTask("2", ACTIVE),
                page.aTask("3", COMPLETED));

        page.clearCompleted();
        page.assertTasks("2");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditByClickOutsideAtAll() {
        page.givenAtAll(ACTIVE, "1");

        page.startEdit("1", "1 edited");
        page.addTaskField.click();
        page.assertTasks("1 edited");
        page.assertItemsLeft(1);
    }

    //buggy test that fails
    @Test
    @Category(Buggy.class)
    public void testEditByPressTabAtAll() {
        page.givenAtAll(page.aTask("1", COMPLETED),
                page.aTask("2", ACTIVE));

        page.confirmEditByTab("2", "2 edited");
        page.assertTasks("1", "2"); //bug
        page.assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAll() {
        page.givenAtAll(page.aTask("1", COMPLETED),
                page.aTask("2", ACTIVE));

        page.cancelEditByEsc("2", "2 edited");
        page.assertTasks("1", "2");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByTextRemoveAtAll() {
        page.givenAtAll(page.aTask("1", COMPLETED),
                page.aTask("2", ACTIVE));

        page.confirmEditByEnter("1", "");
        page.assertTasks("2");
        page.assertItemsLeft(1);
    }
}
