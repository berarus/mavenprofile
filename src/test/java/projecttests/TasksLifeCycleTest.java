package projecttests;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import pages.TodoMVCPage;
import projecttests.categories.Smoke;
import projecttests.config.BaseTest;


/**
 * Created by Anastasia on 3/21/2016.
 */
public class TasksLifeCycleTest extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    @Category(Smoke.class)
    public void testTasksLifeCycle() {
        page.givenAtAll();
        page.add("1");

        page.toggle("1");

        page.filterActive();
        page.assertNoVisibleTasks();

        page.filterCompleted();
        page.confirmEditByEnter("1", "1 edited");

        page.toggle("1 edited");
        page.assertNoVisibleTasks();

        page.filterActive();
        page.toggle("1 edited");
        page.assertNoVisibleTasks();

        page.filterAll();
        page.delete("1 edited");
        page.assertNoTasks();
    }
}
