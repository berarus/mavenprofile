package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

/**
 * Created by Anastasia on 3/31/2016.
 */
public class TodoMVCPage {
    /******************************
     * Test methods and variables *
     ******************************/

    private ElementsCollection tasks = $$("#todo-list li");
    public SelenideElement addTaskField = $("#new-todo");

    @Step
    public void add(String... taskNames) {
        for (String name : taskNames) {
            addTaskField.val(name).pressEnter();
        }
    }

    @Step
    public SelenideElement startEdit(String oldTaskName, String newTaskName) {
        tasks.find(exactText(oldTaskName)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").val(newTaskName);
    }

    @Step
    public void confirmEditByEnter(String oldTaskName, String newTaskName) {
        startEdit(oldTaskName, newTaskName).pressEnter();
    }

    @Step
    public void confirmEditByTab(String oldTaskName, String newTaskName) {
        startEdit(oldTaskName, newTaskName).pressTab();
    }

    @Step
    public void cancelEditByEsc(String oldTaskName, String newTaskName) {
        startEdit(oldTaskName, newTaskName).pressEscape();
    }

    @Step
    public void delete(String taskName) {
        tasks.find(exactText(taskName)).hover().$(".destroy").click();
    }

    @Step
    public void toggle(String taskName) {
        tasks.find(exactText(taskName)).$(".toggle").click();
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
        $("#clear-completed").shouldBe(hidden);
    }

    @Step
    public void assertTasks(String... taskNames) {
        tasks.shouldHave(exactTexts(taskNames));
    }

    @Step
    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    @Step
    public void assertVisibleTasks(String... taskNames) {
        tasks.filter(visible).shouldHave(exactTexts(taskNames));
    }

    @Step
    public void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public void assertItemsLeft(int itemsLeft) {
        $("#todo-count strong").shouldHave(exactText(Integer.toString(itemsLeft)));
    }

    @Step
    public void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    public enum Filter {
        ALL(""),
        ACTIVE("active"),
        COMPLETED("completed");

        private String url;

        Filter(String url) {
            this.url = url;
        }

        public String getURL(){
            return "https://todomvc4tasj.herokuapp.com/#/" + url;
            //"file:///D:/Webinars/Selenide%20Automation/todomvc4tasj/home.html#/"+ url;
        }
    }

    /************
     * Helpers *
     ************/

    public enum TaskType {
        COMPLETED("true"), ACTIVE("false");

        private String flag;

        TaskType(String flag) {
            this.flag = flag;
        }

        public String getFlag() {
            return flag;
        }
    }

    public class Task {
        String taskText;
        TaskType taskType;

        public Task(String taskText, TaskType taskType) {
            this.taskText = taskText;
            this.taskType = taskType;
        }
    }

    @Step
    public Task aTask(String taskText, TaskType taskType) {
        Task task = new Task(taskText, taskType);
        return task;
    }

    @Step
    public void ensurePageOpened(Filter filter) {
        if (!url().equals(filter.getURL())) {
            open(filter.getURL());
        }
    }

    @Step
    public void givenHelper(Filter filter, Task... tasks) {
        ensurePageOpened(filter);
        String elements = "";
        if (tasks.length != 0) {
            for (Task task : tasks) {
                elements += "{\\\"completed\\\":" + task.taskType.getFlag() + ", \\\"title\\\":\\\"" + task.taskText + "\\\"},";
            }
            elements = elements.substring(0, elements.length() - 1);
        }
        executeJavaScript("localStorage.setItem(\"todos-troopjs\", \"[" + elements + "]\")");
        executeJavaScript("location.reload()");
    }

    @Step
    public void givenAtAll(Task... tasks) {
        givenHelper(Filter.ALL, tasks);
    }

    @Step
    public void givenAtActive(Task... tasks) {
        givenHelper(Filter.ACTIVE, tasks);
    }

    @Step
    public void givenAtCompleted(Task... tasks) {
        givenHelper(Filter.COMPLETED, tasks);
    }

    @Step
    public Task[] createTaskList(TaskType taskType, String ... taskTexts){
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new Task(taskTexts[i], taskType);
        }
        return tasks;
    }

    @Step
    public void givenAtAll(TaskType taskType, String... taskTexts) {
        givenHelper(Filter.ALL, createTaskList(taskType, taskTexts));
    }

    @Step
    public void givenAtActive(TaskType taskType, String... taskTexts) {
        givenHelper(Filter.ACTIVE, createTaskList(taskType, taskTexts));
    }

    @Step
    public void givenAtCompleted(TaskType taskType, String... taskTexts) {
        givenHelper(Filter.COMPLETED, createTaskList(taskType, taskTexts));
    }
}
